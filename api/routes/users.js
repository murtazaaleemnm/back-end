const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const User = require("../models/user");

//GET request huleen avah buh hereglegchidiig avch irhed ashiglana
router.get("/", (req, res, next) => {
  User.find()
    .exec()
    .then((docs) => {
      console.log(docs);
      if (docs.length >= 0) {
        res.status(200).json(docs);
      } else {
        res.status(404).json({
          message: "No entries found",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});
//POST request huleen avah
router.post("/", (req, res, next) => {
  //doorh n shine object uusgeed tuund utguudiig onoon yvuulna
  const user = new User({
    _id: mongoose.Types.ObjectId(),
    username: req.body.username,
    password: req.body.passwordN,
  });
  user
    .save()
    .then((result) => {
      console.log(result);
    })
    .catch((err) => console.log(err));
  res.status(201).json({
    message: "Handling POST request to /users",
    createdUser: user,
  });
});

router.get("/:userId", (req, res, next) => {
  const id = req.params.userId;
  User.findById(id)
    .exec()
    .then((doc) => {
      console.log(doc);
      if (doc) {
        res.status(200).json(doc);
      } else {
        res.status(404).json({
          message: "no valid id found",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});


router.patch("/:userId", (req, res, next) => {
  const id = req.params.userId;
  const updateOps = {};
  //request yvyylahda [{"propName":"name", "value":"new value"}] gesen zagvaraar yvuulah
  //doorh for functs tuslamjtaigaar umar ch turliin update avah bolomjtoi bolno
  for (const ops of req.body) {
    updateOps[ops.propName] = ops.value;
  }
  //update functs n uurchluh gj bga object iin id-g avaad uurchluh field iin avna
  User.update({ _id: id }, { $set: updateOps })
    .exec()
    .then((result) => {
      console.log(result);
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

router.delete("/:userId", (req, res, next) => {
  const id = req.params.userId;
  User.remove({ _id: id })
    .exec()
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

module.exports = router;
