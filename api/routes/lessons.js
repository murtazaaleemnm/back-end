const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const Lesson = require("../models/lesson");
const _ = require("lodash");

//GET request huleen avah
router.get("/:userId", (req, res, next) => {
  Lesson.find()
    .exec()
    .then((docs) => {
      console.log(docs);
      if (docs.length >= 0) {
        let grouped = _.groupBy(docs, "user_id");
        res.status(200).json(grouped[req.params.userId]);
      } else {
        res.status(404).json({
          message: "No entries found",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});
//POST request huleen avah
router.post("/:userId", (req, res, next) => {
  //doorh n shine object uusgeed tuund utguudiig onoon yvuulna
  const lesson = new Lesson({
    _id: mongoose.Types.ObjectId(),
    user_id: req.params.userId,
    name: req.body.name,
    code: req.body.code,
  });
  lesson
    .save()
    .then((result) => {
      console.log(result);
    })
    .catch((err) => console.log(err));
  res.status(201).json({
    message: "Handling POST request to /lessons",
    createdLesson: lesson,
  });
});

router.get("/:userId/:lessonId", (req, res, next) => {
  const id = req.params.lessonId;
  Lesson.findById(id)
    .exec()
    .then((doc) => {
      console.log(doc);
      if (doc) {
        res.status(200).json(doc);
      } else {
        res.status(404).json({
          message: "no valid id found",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

router.patch("/:lessonId", (req, res, next) => {
  const id = req.params.lessonId;
  const updateOps = {};
  //request yvyylahda [{"propName":"name", "value":"new value"}] gesen zagvaraar yvuulah
  //doorh for functs tuslamjtaigaar umar ch turliin update avah bolomjtoi bolno
  for (const ops of req.body) {
    updateOps[ops.propName] = ops.value;
  }
  //update functs n uurchluh gj bga object iin id-g avaad uurchluh field iin avna
  Lesson.update({ _id: id }, { $set: updateOps })
    .exec()
    .then((result) => {
      console.log(result);
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

router.delete("/:lessonId", (req, res, next) => {
  const id = req.params.lessonId;
  Lesson.remove({ _id: id })
    .exec()
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

module.exports = router;
