const mongoose = require("mongoose");

const teacherSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  user_id: String,
  name: String,
  code: String,
  lessons: Array,
  grade: String,
});

module.exports = mongoose.model("Teacher", teacherSchema);
